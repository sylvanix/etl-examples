from typing import Optional, List
from pydantic import BaseModel, PositiveInt


class PlaneData(BaseModel):
    """Plane Data model class
    """
    upload_time: str
    uploaded_by: str
    comments: str | None
    flight_id: str
    aircraft_type: str
    pilot_name: str
    pilot_experience: PositiveInt
    pilot_nationality: str
    altitude: List[float]
    latitude: List[float]
    longitude: List[float]


def convert_to_PlaneData(
        upload_time: str,
        uploaded_by: str,
        comments: str,
        flight_id: str,
        sensor_data: dict
    ) -> dict:
    """The data files may contain multiple flight sensor logs; shape the data for a flight
        into a single dict to instantiate a PlaneData instance.
    """
    data = dict()
    data["upload_time"] = upload_time
    data["uploaded_by"] = uploaded_by
    data["comments"] = comments
    data["flight_id"] = flight_id
    data["aircraft_type"] = sensor_data["aircraft_type"]
    data["pilot_name"] = ' '.join([sensor_data["pilot"]["fname"], sensor_data["pilot"]["lname"]])
    data["pilot_experience"] = sensor_data["pilot"]["experience"]
    data["pilot_nationality"] = sensor_data["pilot"]["nationality"]
    data["altitude"] = sensor_data["sensor_log"]["altitude"] 
    data["latitude"] = sensor_data["sensor_log"]["latitude"] 
    data["longitude"] = sensor_data["sensor_log"]["longitude"] 
    
    #print(data)
    pd = PlaneData(**data).model_dump()
    return pd
