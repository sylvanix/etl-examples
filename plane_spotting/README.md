## Airplane Flights Fake Data  

This example does not run in a container, so you'll need to either containerize it yourself, or just run from a Python environment which has the following packages installed:  
   * python-arango  
   * pydantic  
   * xmltodict  
   * faker  
   * dict2xml  

### Dataset Creation  

Run the "create_dataset.py" script to create the "xml_data.txt" file. It also creates the file "json_data.json", just in case you want to play around with it; it's not needed for the ETL.


### Parsing the Flights file and loading to the ArangoDB collection  
Make sure that you have an ArangoDB instance running first, and replace my connection values in "etl.py" with your own. If you don't want to connect to a database, just comment out the calls to the database manager.  

Run the program "etl.py"

### Modifications  
When you looked at how the PlaneData is loaded into the database, you might have noticed that we are allowing ArangoDB to assign each entry its own primary key, which is called "\_key" in ArangoDB. This might not be a problem if we never tried to load the same file twice. If we did that however, then ArangoDB would just load the same data again, but using different primary keys.  
If there is a data member which is unique, meaning that it is not ever expected to be re-used, then we should make that our primary key. In arango, this is done by setting ```collection._key = unique_data_value```. In the PlaneData, flight_id is meant to be a unique identifier, so we should set the primary key equal to that.  
