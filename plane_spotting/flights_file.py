import re
import xmltodict


class FlightsFile(object):
    def __init__(self, filepath: str):
        self.filepath: str = filepath
        self.upload_time: str = None
        self.uploaded_by: str = None
        self.comments: str = None
        self.flight_data: dict = {}
        self._read_file()

    def _read_file(self):
        with open(self.filepath, 'r', encoding="utf-8") as f:
            xml_data = f.read()
        
        # xmltodict will fail if there are multiple root nodes in the XML file,
        # therefore we will strip these root nodes separately before parsing
        # "flight_data" with xmltodict.parse()
        
        # strip off upload_time
        str1 = "<upload_time>"
        str2 = "</upload_time>"
        search_string = str1 +"(.*)"+str2
        self.upload_time = re.search(search_string, xml_data).group(1)
        
        # strip off uploaded_by
        str1 = "<uploaded_by>"
        str2 = "</uploaded_by>"
        search_string = str1 +"(.*)"+str2
        self.uploaded_by = re.search(search_string, xml_data).group(1)
        
        # strip off comments
        str1 = "<comments>"
        str2 = "</comments>"
        search_string = str1 +"(.*)"+str2
        self.comments = re.search(search_string, xml_data).group(1)
        
        # strip off the multi-line flight_data
        str1 = "<flight_data>"
        str2 = "</flight_data>"
        flight_data = str1 + xml_data.split(str1)[1].split(str2)[0] + str2
        self.flight_data = xmltodict.parse(flight_data)
