from database_manager import DBManager
from flights_file import FlightsFile
from plane_data import convert_to_PlaneData


#####################################################
# DATABASE CREATION & CONNECTION
####################################################
flights = DBManager(
    db_url="http://localhost:1234",
    db_name="planes",
    db_user="root",
    db_password="reallysecurepassword",
    collection_name="flights"
).collection


#####################################################
# ETL
####################################################

# Parse the data file
filepath = "xml_data.txt"
ff = FlightsFile(filepath)

# convert the flight sensor data to separate instances of the PlaneData model class
for flight_id in ff.flight_data["flight_data"]:
    print(f"reading sensor data for flight: {flight_id}") 
    pd = convert_to_PlaneData(
        ff.upload_time,
        ff.uploaded_by,
        ff.comments,
        flight_id,
        ff.flight_data["flight_data"][flight_id]
    )

    # insert into the ArangoDB collection named "flights"
    flights.insert(pd)
