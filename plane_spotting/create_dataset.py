import random
from collections import OrderedDict
from copy import deepcopy
from datetime import datetime, timedelta
import json

from faker import Faker
from dict2xml import dict2xml


fake = Faker(['en_US', 'en_GB', 'it_IT'])

Faker.seed(2)

# Set the values for the root level values
t = datetime.now() - timedelta(days=3)
tstr = t.strftime("%Y-%m-%d %H:%M:%S.%f")
file_dict = OrderedDict()
file_dict["upload_time"] = tstr
file_dict["uploaded_by"] = fake.name()
file_dict["comments"] = fake.paragraph(nb_sentences=4, variable_nb_sentences=False)
file_dict["flight_data"] = None

# Define the structure of the nested flight data section
flight_dict = {
    "aircraft_type": None,
    "pilot": {
    "fname": None,
    "lname": None,
    "nationality": None,
    "experience": None
    },
    "sensor_log": {
        "time": [],
        "latitude": [],
        "longitude": [],
        "altitude": [],
        "temperature": []
    }
}

fids = ["AZ2398", "PX1234", "CY8901", "BL3444", "RE2231"]
aircraft_types = ["Blockheed 911", "Nimbus C111", "Aircoach 682"]
data = dict(zip([f for f in fids], [deepcopy(flight_dict) for _ in range(len(fids))]))

for f in fids:
    data[f]["aircraft_type"] = random.choice(aircraft_types)
    fname, lname = fake.name().split(' ')
    data[f]["pilot"]["fname"] = fname
    data[f]["pilot"]["lname"] = lname
    data[f]["pilot"]["nationality"] = fake.country()
    data[f]["pilot"]["experience"] = random.choice([x+1 for x in range(10)])
    for _ in range(15):
        t = fake.past_datetime(start_date="-45d")
        tstr = t.strftime("%Y-%m-%d %H:%M:%S.%f")
        data[f]["sensor_log"]["time"].append(tstr)
        data[f]["sensor_log"]["latitude"].append(float(fake.latitude()))
        data[f]["sensor_log"]["longitude"].append(float(fake.longitude()))
        data[f]["sensor_log"]["altitude"].append(random.uniform(10e3, 40e3))
        data[f]["sensor_log"]["temperature"].append(random.uniform(2.0, 12.0))

# finally, add the flight data to the dictionary
file_dict["flight_data"] = data 

##################################################################################################

# write out the dictionary as an XML file
xml = dict2xml(file_dict)
outfile = "xml_data.txt"
with open(outfile, 'w') as f:
    f.write(xml)


# write out the dictionary as a JSON file
outfile = "json_data.json"
with open(outfile, 'w') as f:
    f.write(json.dumps(file_dict))

