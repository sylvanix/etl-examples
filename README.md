## ETL Examples  


 example 1: [Create and Parse a fake XML dataset before loading to database](./plane_spotting)  
 
  example 2: [Containerized ArangoDB ETLs](./arango_and_pydantic)  