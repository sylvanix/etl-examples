# specify a base image
FROM python:3.12.1-bookworm

USER root

#define the user - get UID & GID from the id command
ARG USERNAME=your_user_name
ARG USERID=1000
ARG GNAME="your_user_name"
ARG GID=1000


# always combine "update" with "install" in a RUN statement
RUN apt-get update && apt-get install -y --no-install-recommends \
                                      python3-pip \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* \
    && groupadd -g $GID $GNAME \
    && useradd -ms /bin/bash --uid $USERID --gid $GID \
               --groups $GNAME $USERNAME


# make the /data dir in the container (this can be a mapped host dir in the docker run script)
RUN mkdir /data

# copy files to the container
WORKDIR /workspace
COPY ./requirements.txt /workspace

USER $USERNAME

# install Python modules
RUN pip3 install --upgrade pip \
    && pip3 install --no-cache-dir -r /workspace/requirements.txt

# copy the python files to the container
COPY ./app/ /workspace

# entrypoint
CMD ["python3", "main.py"]
