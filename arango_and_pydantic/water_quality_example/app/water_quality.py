"""
The schema class for the Water Quality data
"""

from pydantic import BaseModel, PositiveInt
from datetime import datetime, timezone


class WaterQuality(BaseModel):
    ph: float | None
    Hardness: float | None
    Solids: float | None
    Chloramines: float | None
    Sulfate: float | None
    Conductivity: float | None
    Organic_carbon: float | None
    Trihalomethanes: float | None
    Turbidity: float | None
    Potability: bool | None
    date_added: str | None


def convert_to_WaterQuality(data: dict) -> dict:
    utc_datetime = datetime.now(timezone.utc)
    data["date_added"] = utc_datetime.strftime("%Y-%m-%d %H:%M:%S")
    wq = WaterQuality(**data).model_dump()
    return wq
