## Setting up ArangoDB  

Let's look at the docker-compose file [docker-compose-arangodb.yml](./docker-compose-arangodb.yml):  


```docker-compose
version: '3.7'

services:
  arangodb:
    image: arangodb:3.10
    container_name: "arangodb"
    restart: unless-stopped
    ports:
      - 1234:8529
    networks:
      - test-net
    #environment:
    #  ARANGO_USER: root
    #  ARANGO_ROOT_PASSWORD: reallysecurepassword
    volumes:
      - $PWD/volumes/arango-etl:/var/lib/arangodb3
    secrets:
      - db_secret     

secrets:
  db_secret:
    file: ./.secrets

networks:
  test-net:
    external:
      name: test-net
```

Notice that the environment variables are commented-out. We are reading them instead from the file ".secrets", shown below:  

```
POSTGRES_USER=meowth
POSTGRES_PASSWORD=thatsright
POSTGRES_DB=practical

PGADMIN_DEFAULT_EMAIL=meowth@rocket.org
PGADMIN_DEFAULT_PASSWORD=thatsright

ARANGO_USER=root
ARANGO_ROOT_PASSWORD=reallysecurepassword
```

Build and run ArangoDB:  

```Bash
bash docker-compose -f docker-compose-arangodb.yml up -d
```

The ArangoDB UI can be accessed from localhost:1234:  

![alt text](./images/arango_ui.png "ArangoDB UI")  


## Running one of the ETLs  

With the database service running, let's look at the ETL code and run it.  

The Dockerfile looks the same as the one for the Postgres ETL example above, but the other files are different. The build_run.sh file uses a similar structure to the previous one, but the environment variables are for connecting to an ArangoDB database.  

```Bash
#!/bin/bash

source /home/your_user_name/.secrets

# build
IMAGENAME="water_quality_etl"
TAG="0.0.1"
docker build -t $IMAGENAME:$TAG .

# run
HOSTDATA="/home/your_user_name/data"
docker run \
    --net=host \
    --user=1000:1000 \
    -v /etc/localtime:/etc/localtime:ro \
    -v $HOSTDATA:/data \
    --env PYTHONBUFFERED=1 \
    --env DB_URL=$ADB_URL \
    --env DB_NAME=water \
    --env DB_USER=$ADB_USER \
    --env DB_PASSWORD=$ADB_PASSWORD \
    --env DATA_DIR=/data \
    $IMAGENAME:$TAG
```

This Bash script sources my .secrets file to get the values for DB_URL, DB_USER, and DB_PASSWORD, but you could, while testing, enter those values straight into this bash file.  

You can always run the app using Root DB\_NAME and DB\_PASSWORD, but to run as another user, you must, through the ArangoDB UI, add the new user and edit the collection so that this user had permission to read, write, and edit.  

The Python app looks in the HOSTDATA directory for a file named ""water_potability.csv". So, put that file (included with the files here) in that directory.  

Now build and run the app:

```Bash
bash build_run.sh
```

You should get output similar to the following:  

```
water_quality_example$ bash build_run.sh 
[+] Building 0.5s (13/13) FINISHED                                                       docker:default
 => [internal] load .dockerignore                                                                  0.0s
 => => transferring context: 2B                                                                    0.0s
 => [internal] load build definition from Dockerfile                                               0.0s
 => => transferring dockerfile: 1.03kB                                                             0.0s
 => [internal] load metadata for docker.io/library/python:3.11.5-bookworm                          0.4s
 => [auth] library/python:pull token for registry-1.docker.io                                      0.0s
 => [internal] load build context                                                                  0.0s
 => => transferring context: 91B                                                                   0.0s
 => [1/7] FROM docker.io/library/python:3.11.5-bookworm@sha256:844b3044eef9d990d3c640e046241ac396  0.0s
 => CACHED [2/7] RUN apt-get update && apt-get install -y --no-install-recommends                  0.0s
 => CACHED [3/7] RUN mkdir /data                                                                   0.0s
 => CACHED [4/7] WORKDIR /workspace                                                                0.0s
 => CACHED [5/7] COPY ./requirements.txt /workspace                                                0.0s
 => CACHED [6/7] RUN pip3 install --upgrade pip     && pip3 install --no-cache-dir -r /workspace/  0.0s
 => CACHED [7/7] COPY ./app/ /workspace                                                            0.0s
 => exporting to image                                                                             0.0s
 => => exporting layers                                                                            0.0s
 => => writing image sha256:3bfdba56735f04fd5de5ebfcdd0e7be55dda9c0f7fa506d3a8e0b084bc2a3003       0.0s
 => => naming to docker.io/library/water_quality_etl:0.0.1
```

